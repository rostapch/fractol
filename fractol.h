/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 16:05:14 by rostapch          #+#    #+#             */
/*   Updated: 2017/07/04 18:49:15 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include "mlx.h"
# include "math.h"
# include "libft/libft.h"
# include "libft/get_next_line.h"
# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>
# include <fcntl.h>
# include <complex.h>
# include "X.h"

# define W_W			800
# define W_H			800
# define ITER			100

# define XX				2.2
# define XN				-2.2
# define YX				2.2
# define YN				-2.2

# define KEY_LEFT		123
# define KEY_RIGHT		124
# define KEY_UP			126
# define KEY_DOWN		125

# define ESC_KEY		53

# define R_MB			2
# define L_MB			1
# define MW_UP			4
# define MW_DOWN		5

typedef struct			s_img
{
	void				*imgg;
	int					size_line;
	int					bpp;
	int					width;
	int					height;
	int					type;
	int					format;
	int					byteorder;
	char				*data;
}						t_img;

typedef struct			s_window
{
	t_img				*img;
	void				*mlx;
	void				*wnd;
	int					c[3];
	int					i;
}						t_window;

typedef struct			s_d
{
	double				zx;
	double				zy;
	double				cx;
	double				cy;
	double				buff;
	int					i;
	int					j;
	int					n;
	double				mvx;
	double				mvy;
	double				zoom;
}						t_d;

double					zoom(double z, int init);
int						scale(int button, int x, int y, double xy[2]);
int						key_press(int key, void *param);
int						m_key_press(int button, int x, int y, void *param);
void					def_window(t_window *mw, int init, int cl[3], int ints);
int						fract(t_window *mw, double zoom, double movexy[2],
						int index);
int						mouse_move(int x, int y, void *param);
void					put_pixel_img(t_img *img, int x, int y, int c);
void					jope(int x, int y, int qwerty, double ret[2]);
void					mandelbrot(t_window mw, double zoom, double movex,
						double movey);
void					julia(t_window mw, double zoom, double movexy[2],
						double cxy[2]);
void					ship(t_window mw, double zoom, double movex,
						double movey);
int						key_events(int key, double xy[2], t_window *mw);
int						exit_x(void *par);

#endif
