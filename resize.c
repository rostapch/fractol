/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resize.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/04 17:24:12 by rostapch          #+#    #+#             */
/*   Updated: 2017/07/04 18:54:54 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

double	zoom(double z, int init)
{
	static double zoom;

	if (init == 1)
		zoom = z;
	return (zoom);
}

int		scale(int button, int x, int y, double xy[2])
{
	if (button == 5)
		zoom(zoom(0, 0) / 1.2, 1);
	else if (button == 4)
	{
		x -= W_W / 2;
		y -= W_H / 2;
		xy[0] += ((double)x * 1.5) / zoom(0, 0) / ((double)W_W * 0.5);
		xy[1] -= (double)y / zoom(0, 0) / ((double)W_H * 0.5);
		zoom(zoom(0, 0) * 1.2, 1);
		xy[0] -= ((double)x * 1.5) / zoom(0, 0) / ((double)W_W * 0.5);
		xy[1] += (double)y / zoom(0, 0) / ((double)W_H * 0.5);
	}
	return (0);
}
