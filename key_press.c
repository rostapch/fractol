/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_press.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 16:19:00 by rostapch          #+#    #+#             */
/*   Updated: 2017/07/04 17:08:26 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	key_events2(int key, double xy[2], t_window *mw)
{
	int	buff;

	buff = 0;
	if (key == KEY_RIGHT)
	{
		buff = mw->c[0];
		mw->c[0] = mw->c[1];
		mw->c[1] = mw->c[2];
		mw->c[2] = buff;
	}
	else if (key == KEY_LEFT)
	{
		buff = mw->c[2];
		mw->c[2] = mw->c[1];
		mw->c[1] = mw->c[0];
		mw->c[0] = buff;
	}
	if (key == KEY_LEFT || key == KEY_RIGHT)
	{
		mw->c[0] = mw->c[0] == 0 ? 1 : 0;
		mw->c[1] = mw->c[1] == 0 ? 1 : 0;
		mw->c[2] = mw->c[2] == 0 ? 1 : 0;
		def_window(mw, 1, mw->c, mw->i);
		key_events(15, xy, mw);
	}
}

int		key_events1(int key, double xy[2], t_window *mw)
{
	if (key == KEY_UP)
	{
		def_window(mw, 1, mw->c, mw->i + 1);
		key_events(15, xy, mw);
	}
	else if (key == KEY_DOWN)
	{
		def_window(mw, 1, mw->c, mw->i - 1);
		key_events(15, xy, mw);
	}
	else if (key == 91)
		xy[1] += 1 / zoom(0, 0);
	else if (key == 86)
		xy[0] -= 1 / zoom(0, 0);
	else if (key == 84)
		xy[1] -= 1 / zoom(0, 0);
	else if (key == 88)
		xy[0] += 1 / zoom(0, 0);
	if (key == 91 || key == 86 || key == 84 || key == 88)
		fract(mw, zoom(0, 0), xy, -1);
	else
		(key_events2(key, xy, mw));
	return (1);
}

int		key_events(int key, double xy[2], t_window *mw)
{
	void	*temp;

	temp = NULL;
	if (key == ESC_KEY)
		exit(0);
	else if (key == 43)
	{
		m_key_press(-666, xy[0], xy[1], temp);
		fract(mw, zoom(1, 1), xy, 42);
	}
	else if (key == 47)
	{
		m_key_press(-666, xy[0], xy[1], temp);
		fract(mw, zoom(1, 1), xy, 13);
	}
	else if (key == 15)
	{
		m_key_press(-666, xy[0], xy[1], temp);
		fract(mw, zoom(1, 1), xy, -1);
	}
	else
		return (key_events1(key, xy, mw));
	return (1);
}

int		key_press(int key, void *param)
{
	static t_window	mw;
	double			xy[2];

	xy[0] = 0;
	xy[1] = 0;
	(void)param;
	def_window(&mw, 0, mw.c, 0);
	if (key_events(key, xy, &mw) < 0)
		;
	return (0);
}
