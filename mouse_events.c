/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_events.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/04 17:27:03 by rostapch          #+#    #+#             */
/*   Updated: 2017/07/04 18:10:30 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		exit_x(void *par)
{
	par = NULL;
	exit(1);
	return (0);
}

void	jope(int x, int y, int qwerty, double ret[2])
{
	static double cxy[2];

	if (qwerty == 1 && x > 0 && y > 0 && x <= W_W && y <= W_H)
	{
		cxy[0] = (double)((x + W_W * 10) % W_W - W_W / 2) / W_W;
		cxy[1] = (double)((y + W_H * 10) % W_H - W_H / 2) / W_H;
	}
	ret[0] = cxy[0];
	ret[1] = cxy[1];
}

int		mouse_move(int x, int y, void *param)
{
	double				tmp[2];
	static t_window		mw;

	def_window(&mw, 0, mw.c, 0);
	(void)param;
	jope(x, y, 1, tmp);
	if (fract(&mw, 1, tmp, -13) == 3)
	{
		mlx_clear_window(mw.mlx, mw.wnd);
		fract(&mw, zoom(0, 0), tmp, -1);
	}
	return (0);
}

int		m_key_press(int button, int x, int y, void *param)
{
	static t_window		mw;
	static double		xy[2];

	(void)param;
	def_window(&mw, 0, mw.c, 0);
	xy[0] = button == -666 ? 0 : xy[0];
	xy[1] = button == -666 ? 0 : xy[1];
	if (fract(&mw, zoom(0, 0), xy, -13) == 2)
	{
		scale(button, x, y, xy);
		fract(&mw, zoom(0, 0), xy, -1);
	}
	if (button == MW_UP || button == MW_DOWN || button == 1)
	{
		xy[0] += ((double)(x - W_W / 2) / W_W) * ((XX - XN) / zoom(0, 0));
		xy[1] -= ((double)(y - W_H / 2) / W_H) * ((YX - YN) / zoom(0, 0));
		mlx_clear_window(mw.mlx, mw.wnd);
	}
	if (button == MW_UP || button == 1)
		fract(&mw, zoom(zoom(0, 0) * 1.2, 1), xy, -1);
	else if (button == MW_DOWN)
		fract(&mw, zoom(zoom(0, 0) / 1.2, 1), xy, -1);
	return (0);
}
