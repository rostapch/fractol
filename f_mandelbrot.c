/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/04 18:12:41 by rostapch          #+#    #+#             */
/*   Updated: 2017/07/04 18:52:04 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	build_m(t_d dt, t_window mw)
{
	while (++dt.i < W_H)
	{
		dt.cy = ((YX - YN) / (dt.zoom * W_H)) * dt.i + YN / dt.zoom - dt.mvy;
		dt.j = -1;
		while (++dt.j < W_W)
		{
			dt.zx = 0.0;
			dt.zy = 0.0;
			dt.cx = ((XX - XN) / (dt.zoom * W_W)) * dt.j +
			XN / dt.zoom + dt.mvx;
			dt.n = -1;
			while (++dt.n < ITER && (dt.zx * dt.zx + dt.zy * dt.zy < 4))
			{
				dt.buff = 2 * dt.zx * dt.zy + dt.cy;
				dt.zx = dt.zx * dt.zx - dt.zy * dt.zy + dt.cx;
				dt.zy = dt.buff;
			}
			if (dt.n != ITER && dt.n != 0)
				put_pixel_img(mw.img, dt.j, dt.i, ft_rgb(dt.n * mw.c[0] *
					mw.i, dt.n * mw.c[1] * mw.i, dt.n * mw.c[2] * mw.i));
		}
	}
}

void	mandelbrot(t_window mw, double zoom, double movex, double movey)
{
	t_d	dt;

	dt.zx = 0;
	dt.zy = 0;
	dt.cx = 0;
	dt.cy = 0;
	dt.buff = 0;
	dt.mvx = movex;
	dt.mvy = movey;
	dt.zoom = zoom;
	mw.img = (t_img*)malloc(sizeof(t_img));
	mw.img->imgg = mlx_new_image(mw.mlx, W_W, W_H);
	mw.img->data = mlx_get_data_addr(mw.img->imgg, &mw.img->bpp,
		&mw.img->size_line, &mw.img->byteorder);
	dt.i = -1;
	build_m(dt, mw);
	mlx_put_image_to_window(mw.mlx, mw.wnd, mw.img->imgg, 0, 0);
	ft_bzero(mw.img->data, W_H * W_W * mw.img->bpp / 8);
}
