# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/04/03 17:56:15 by rostapch          #+#    #+#              #
#    Updated: 2017/07/04 18:13:22 by rostapch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror

NAME = fractol

FRAMEFLAGS = -framework OpenGL -framework AppKit

INCLUDE_LIB = -L libft -l ft -L minilibx_macos_2016 -lmlx

HEADERS = -I/opt/X11/include/X11  -I . -I libft -I minilibx

SRC_C =	key_press.c main.c resize.c mouse_events.c f_mandelbrot.c f_julia.c f_bship.c

OBJ = $(SRC_C:.c=.o)

.PHONY: all lib lib_re lib_clean lib_fclean libx libx_clean libx_re\
	fractol_clean clean fractol_fclean fclean re
.SUFFIXES: .c .o


all: libx lib $(OBJ)
	$(CC) $(CFLAGS) $(HEADERS) $(INCLUDE_LIB) $(FRAMEFLAGS) -o $(NAME) $(OBJ)

$(NAME): all

%.o: %.c
	$(CC) -c $(CFLAGS) $(HEADERS)  -o $@ $<

lib:
	make -C libft

lib_re:
	make re -C libft

lib_clean:
	make clean -C libft

lib_fclean:
	make fclean -C libft



libx:
	make -C minilibx_macos_2016
	make -C minilibx

libx_clean:
	make clean -C minilibx_macos_2016
	make clean -C minilibx

libx_re:
	make re -C minilibx_macos_2016
	make re -C minilibx



fractol_clean:
	/bin/rm -f $(OBJ)

fractol_fclean: fractol_clean
	/bin/rm -f $(NAME)

clean: lib_clean libx_clean
	/bin/rm -f $(OBJ)



fclean: lib_fclean clean
	/bin/rm -f $(NAME)

re: fclean all