/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <rostapch@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/06 18:41:52 by rostapch          #+#    #+#             */
/*   Updated: 2017/07/04 18:15:16 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	put_pixel_img(t_img *img, int x, int y, int c)
{
	if (0 <= x && x < W_W)
		if (0 <= y && y < W_H)
			*(int *)(img->data + (x + y * W_W) * img->bpp / 8) = c;
}

void	def_window(t_window *mw, int init, int cl[3], int i)
{
	static t_window data;

	if (init == 1)
	{
		data.mlx = mw->mlx;
		data.wnd = mw->wnd;
		data.c[0] = cl[0];
		data.c[1] = cl[1];
		data.c[2] = cl[2];
		if (i >= 1)
		{
			data.i = i;
		}
	}
	mw->mlx = data.mlx;
	mw->wnd = data.wnd;
	mw->c[0] = data.c[0];
	mw->c[1] = data.c[1];
	mw->c[2] = data.c[2];
	mw->i = data.i;
}

int		fract(t_window *mw, double zoom, double movexy[2], int index)
{
	static int	fractl;
	double		tmp[2];

	if (index == -13)
		return (fractl + 1);
	if (index >= 0 && index <= 2)
		fractl = index;
	else if (index == 42)
		fractl = (fractl + 1) % 3;
	else if (index == 13)
		fractl = (fractl + 2) % 3;
	if (fractl == 0)
		mandelbrot(*mw, zoom, movexy[0], movexy[1]);
	else if (fractl == 2)
	{
		tmp[0] = 0;
		tmp[1] = 0;
		jope(0, 0, 0, tmp);
		julia(*mw, zoom, movexy, tmp);
	}
	else
		ship(*mw, zoom, movexy[0], movexy[1]);
	return (0);
}

int		exit_error(int argc, char **argv)
{
	if (argc == 2 && ft_strlen(argv[1]) == 1 && ft_isdigit(argv[1][0]))
		if (ft_atoi(argv[1]) >= 0 && ft_atoi(argv[1]) <= 2)
			return (0);
	printf("\n\tIncorrect input!\n\n./fractol [index]\n where index is:");
	printf("\n\t0 for mandelbrot\n\t1 for burning ship\n\t2 for julia\n");
	return (-1);
}

int		main(int argc, char **argv)
{
	t_window	mw;
	int			index;
	double		xy[2];

	if (argc == 2 && ft_strlen(argv[1]) == 1 && ft_isdigit(argv[1][0]))
		if ((index = ft_atoi(argv[1])) >= 0 && index <= 2)
		{
			mw.mlx = mlx_init();
			mw.wnd = mlx_new_window(mw.mlx, W_W, W_H, "fractol");
			mw.c[0] = 1;
			mw.c[1] = 0;
			mw.c[2] = 0;
			mw.i = 5;
			xy[1] = 0;
			xy[0] = 0;
			def_window(&mw, 1, mw.c, 2);
			fract(&mw, zoom(1, 1), xy, index);
			fract(&mw, zoom(1, 1), xy, -1);
			mlx_hook(mw.wnd, KeyPress, KeyPressMask, key_press, &mw);
			mlx_hook(mw.wnd, ButtonPress, ButtonPressMask, m_key_press, &mw);
			mlx_hook(mw.wnd, 6, 0, mouse_move, &mw);
			mlx_hook(mw.wnd, 17, 1L << 17, exit_x, &mw);
			mlx_loop(mw.mlx);
		}
	return (exit_error(argc, argv));
}
